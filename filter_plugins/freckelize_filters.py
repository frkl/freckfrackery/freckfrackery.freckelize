#!/usr/bin/python


class FilterModule(object):

    def filters(self):
        return {
            "join_folders_filter": self.join_folders_filter,
            "true_if_empty": self.true_if_empty_filter,
            "false_if_empty": self.false_if_empty_filter
        }

    def join_folders_filter(self, folders, token):

        return token.join(folders)

    def true_if_empty_filter(self, value):

        if not value:
            return True
        else:
            return False

    def false_if_empty_filter(self, value):

        if not value:
            return False
        else:
            return True





